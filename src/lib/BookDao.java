package lib;

import java.util.List;



public interface  BookDao {
	
	public List<Book> GetAllBook();
	
	public List<Book> GetById(String isbn);
	
	

}
