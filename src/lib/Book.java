package lib;

public class Book {
	
	private Integer id=0;
	private String title="";
	private String isbn="";
	private String author="";
	private String myabstract="";
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getMyabstract() {
		return myabstract;
	}
	public void setMyabstract(String myabstract) {
		this.myabstract = myabstract;
	}
}
