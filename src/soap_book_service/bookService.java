package soap_book_service;


import java.util.ArrayList;
import java.util.List;

import lib.Book;
import lib.BookDaoImpl;


public class bookService {
	
	private Book bk=null;
	private List<Book> list_book=null;
	private List<Book> Mylist_book=null;
	
	
	public List<Book> getBookByIsbn(String isbn) {
		// TODO Auto-generated method stub
		BookDaoImpl bookdao = new BookDaoImpl();
		
		list_book=new ArrayList<Book>();
		list_book=bookdao.GetById(isbn);
		Mylist_book=new ArrayList<Book>();
	   
		

		if (list_book != null&&list_book.size()>0) {
			
			bk=new Book();
			
			for (Book b : list_book) {

				
				 
				 bk=new Book();
				 bk.setAuthor(b.getAuthor());
				 bk.setId(b.getId());
				 bk.setIsbn(b.getIsbn());
				 bk.setTitle(b.getTitle());
				 bk.setMyabstract(b.getMyabstract());

			}
			Mylist_book.add(bk);
		}
		return list_book;
	}
	
	
	public List<Book> getAllBook() {
		
		BookDaoImpl bookdao = new BookDaoImpl();
		
		list_book=new ArrayList<Book>();
		list_book=bookdao.GetAllBook();
		
		return list_book;
	}
	public String getMyName(String name) {
		return "Hello, My name is "+name;
	}
	

	


}
